package ru.t1.ktubaltseva.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.component.Bootstrap;

public class LoggerApp {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}