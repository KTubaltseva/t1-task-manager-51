package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDTOService extends AbstractUserOwnedWBSDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<TaskDTO> resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository modelRepository = getRepository(entityManager);
            resultModel = modelRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

}
