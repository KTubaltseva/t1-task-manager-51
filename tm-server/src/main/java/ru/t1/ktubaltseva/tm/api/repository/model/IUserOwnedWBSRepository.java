package ru.t1.ktubaltseva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModelWBS;

public interface IUserOwnedWBSRepository<M extends AbstractUserOwnedModelWBS> extends IUserOwnedRepository<M> {

    @NotNull
    M create(
            @NotNull String userId,
            @NotNull String name
    ) throws InstantiationException, IllegalAccessException;

    @NotNull
    M create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws InstantiationException, IllegalAccessException;

}
