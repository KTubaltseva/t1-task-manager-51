package ru.t1.ktubaltseva.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.log.OperationEvent;
import ru.t1.ktubaltseva.tm.log.OperationType;

@NoArgsConstructor
public final class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    @Nullable
    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister entityPersister) {
        return false;
    }

    private void log(final OperationType operationType, final Object entity) {
        if (jmsLoggerProducer == null) return;
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}

