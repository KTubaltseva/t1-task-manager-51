package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelWBSDTO;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface IUserOwnedWBSDTOService<M extends AbstractUserOwnedModelWBSDTO> extends IUserOwnedDTOService<M> {

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name
    ) throws AbstractException, InstantiationException, IllegalAccessException;

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException, InstantiationException, IllegalAccessException;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws AbstractException;

    @NotNull
    M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException;

    @NotNull
    M update(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException;

}
